export const AUTH_DOMAIN = process.env.REACT_APP_AUTH_DOMAIN || "";
export const AUTH_CLIENT_ID = process.env.REACT_APP_AUTH_CLIENT_ID || "";
