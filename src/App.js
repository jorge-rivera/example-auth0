import logo from "./logo.svg";
import { useAuth0 } from "@auth0/auth0-react";
import Login from "./components/Login";
import Logout from "./components/Logout";
import Profile from "./components/Profile";
import "./App.css";

function App() {
  const { isAuthenticated } = useAuth0();
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        {!isAuthenticated && <Login />}
        {isAuthenticated && (
          <>
            <Profile />
            <Logout />
          </>
        )}
      </header>
    </div>
  );
}

export default App;
